async function connect(){
    if(global.connection && global.connection.state !== 'disconnected')
        return global.connection;

    const mysql = require("mysql2/promise");
    const connection = await mysql.createConnection("mysql://academico:123456@localhost:3306/academico");
    console.log("Conectou no MySQL!");
    global.connection = connection;
    return connection;
}

async function selectAlunos(){
    const conn = await connect();
    const [rows] = await conn.query('SELECT * FROM alunos;');
    //console.log(rows);
    return rows;
}
 


// connect to mysql
function consultaBD($query){

}

module.exports = {selectAlunos, consultaBD}
